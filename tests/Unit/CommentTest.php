<?php

namespace Tests\Unit;

use App\Article;
use App\Comment;
use Tests\TestCase;;

class CommentTest extends TestCase
{
    /**
     * A basic unit test example.
     * @group comments
     * @return void
     */
    public function testExample()
    {
        $this->assertTrue(true);
    }

    /**
     * A basic unit test example.
     * @group comments
     * @return void
     */
    public function testSuccessShowAllcomments()
    {
        $response = $this->json('get', route('comments.index'));
        $response->assertStatus(200);
    }

    /**
     * A basic unit test example.
     * @group comments
     * @return void
     */
    public function testSuccessCreateCommentIfAuth()
    {
        $this->actingAs($this->user, 'api');
        $article = factory(Article::class)->create();

        $data = [
            'article_id' => $article->id,
            'body' => $this->faker->text,
            'user_id' => $this->user->id
        ];

        $response = $this->json('post', route('comments.store'), $data);

        $response->assertCreated();
        $this->assertDatabaseHas('comments', $data);
        $resp = json_decode($response->getContent());
        $this->assertEquals($data['article_id'], $resp->data->article->id);
        $this->assertEquals($data['body'], $resp->data->body);
        $this->assertEquals($data['user_id'], $resp->data->user->id);
    }

    /**
     * A basic unit test example.
     * @group comments
     * @return void
     */
    public function testFailedCreateCommentIfNotAuth()
    {
        $article = factory(Article::class)->create();

        $data = [
            'article_id' => $article->id,
            'body' => $this->faker->text,
            'user_id' => $this->user->id
        ];

        $response = $this->json('post', route('comments.store'), $data);

        $response->assertStatus(401);
    }

    /**
     * A basic unit test example.
     * @group comments
     * @return void
     */
    public function testArticleIdRequiredValidationIfAuth()
    {
        $this->actingAs($this->user, 'api');
        $data = [
            'article_id' => '',
            'body' => $this->faker->text,
            'user_id' => $this->user->id
        ];
        $response = $this->json(
            'post',
            route('comments.store'),
            $data
        );
        $response->assertStatus(422);

        $this->assertDatabaseMissing('comments', $data);
        $resp = json_decode($response->getContent());
        $this->assertTrue(array_key_exists('message', (array)$resp));
        $this->assertTrue(array_key_exists('errors', (array)$resp));
        $this->assertTrue(array_key_exists('article_id', (array)$resp->errors));
        $this->assertFalse(array_key_exists('body', (array)$resp->errors));
        $this->assertFalse(array_key_exists('user_id', (array)$resp->errors));
    }

    /**
     * A basic unit test example.
     * @group comments
     * @return void
     */
    public function testArticleIdExistsInDatabaseValidationIfAuth()
    {
        $this->actingAs($this->user, 'api');
        $data = [
            'article_id' => '33333333333333333333333333333',
            'body' => $this->faker->text,
            'user_id' => $this->user->id
        ];
        $response = $this->json(
            'post',
            route('comments.store'),
            $data
        );
        $response->assertStatus(422);

        $this->assertDatabaseMissing('comments', $data);
        $resp = json_decode($response->getContent());
        $this->assertTrue(array_key_exists('message', (array)$resp));
        $this->assertTrue(array_key_exists('errors', (array)$resp));
        $this->assertFalse(array_key_exists('user_id', (array)$resp->errors));
        $this->assertTrue(array_key_exists('article_id', (array)$resp->errors));
        $this->assertFalse(array_key_exists('body', (array)$resp->errors));
    }

    /**
     * A basic unit test example.
     * @group comments
     * @return void
     */
    public function testBodyRequiredValidationIfAuth()
    {
        $this->actingAs($this->user, 'api');
        $article = factory(Article::class)->create();

        $data = [
            'article_id' => $article->id,
            'body' => '',
            'user_id' => $this->user->id
        ];
        $response = $this->json(
            'post',
            route('comments.store'),
            $data
        );
        $response->assertStatus(422);

        $this->assertDatabaseMissing('comments', $data);
        $resp = json_decode($response->getContent());
        $this->assertTrue(array_key_exists('message', (array)$resp));
        $this->assertTrue(array_key_exists('errors', (array)$resp));
        $this->assertTrue(array_key_exists('body', (array)$resp->errors));
        $this->assertFalse(array_key_exists('article_id', (array)$resp->errors));
        $this->assertFalse(array_key_exists('user_id', (array)$resp->errors));
    }

    /**
     * A basic unit test example.
     * @group comments
     * @return void
     */
    public function testBodyMinCharactersValidationIfAuth()
    {
        $this->actingAs($this->user, 'api');
        $article = factory(Article::class)->create();

        $data = [
            'article_id' => $article->id,
            'body' => 'a',
            'user_id' => $this->user->id
        ];
        $response = $this->json(
            'post',
            route('comments.store'),
            $data
        );
        $response->assertStatus(422);

        $this->assertDatabaseMissing('comments', $data);
        $resp = json_decode($response->getContent());
        $this->assertTrue(array_key_exists('message', (array)$resp));
        $this->assertTrue(array_key_exists('errors', (array)$resp));
        $this->assertTrue(array_key_exists('body', (array)$resp->errors));
        $this->assertFalse(array_key_exists('article_id', (array)$resp->errors));
        $this->assertFalse(array_key_exists('user_id', (array)$resp->errors));
    }

    /**
     * A basic unit test example.
     * @group comments
     * @return void
     */
    public function testUserIdRequiredValidationIfAuth()
    {
        $this->actingAs($this->user, 'api');
        $article = factory(Article::class)->create();

        $data = [
            'article_id' => $article->id,
            'body' => $this->faker->text,
            'user_id' => ''
        ];
        $response = $this->json(
            'post',
            route('comments.store'),
            $data
        );
        $response->assertStatus(422);

        $this->assertDatabaseMissing('comments', $data);
        $resp = json_decode($response->getContent());
        $this->assertTrue(array_key_exists('message', (array)$resp));
        $this->assertTrue(array_key_exists('errors', (array)$resp));
        $this->assertTrue(array_key_exists('user_id', (array)$resp->errors));
        $this->assertFalse(array_key_exists('article_id', (array)$resp->errors));
        $this->assertFalse(array_key_exists('body', (array)$resp->errors));
    }

    /**
     * A basic unit test example.
     * @group comments
     * @return void
     */
    public function testUserIdExistsInDatabaseValidationIfAuth()
    {
        $this->actingAs($this->user, 'api');
        $article = factory(Article::class)->create();

        $data = [
            'article_id' => $article->id,
            'body' => $this->faker->text,
            'user_id' => '3333333333333333333333333333333'
        ];
        $response = $this->json(
            'post',
            route('comments.store'),
            $data
        );
        $response->assertStatus(422);

        $this->assertDatabaseMissing('comments', $data);
        $resp = json_decode($response->getContent());
        $this->assertTrue(array_key_exists('message', (array)$resp));
        $this->assertTrue(array_key_exists('errors', (array)$resp));
        $this->assertTrue(array_key_exists('user_id', (array)$resp->errors));
        $this->assertFalse(array_key_exists('article_id', (array)$resp->errors));
        $this->assertFalse(array_key_exists('body', (array)$resp->errors));
    }

    /**
     * A basic unit test example.
     * @group comments
     * @return void
     */
    public function testSuccessShowCommentIfAuth()
    {
        $this->actingAs($this->user, 'api');
        $comment = factory(Comment::class)->create();

        $response = $this->json(
            'get',
            route('comments.show', ['comment' => $comment])
        );
        $response->assertStatus(200);
        $resp = json_decode($response->getContent());
        $this->assertEquals($comment->article_id, $resp->data->article->id);
        $this->assertEquals($comment->body, $resp->data->body);
        $this->assertEquals($comment->user_id, $resp->data->user->id);
    }

    /**
     * A basic unit test example.
     * @group comments
     * @return void
     */
    public function testFailedShowCommentIfNotAuth()
    {
        $comment = factory(Comment::class)->create();

        $response = $this->json(
            'get',
            route('comments.show', ['comment' => $comment])
        );
        $response->assertStatus(401);
    }

    /**
     * A basic unit test example.
     * @group comments
     * @return void
     */
    public function testSuccessUpdateCommentIfAuth()
    {
        $this->actingAs($this->user, 'api');
        $article = factory(Article::class)->create();
        $comment = factory(Comment::class)->create();

        $data = [
            'article_id' => $article->id,
            'body' => $this->faker->text,
            'user_id' => $this->user->id
        ];
        $response = $this->json(
            'put',
            route('comments.update', ['comment' => $comment]),
            $data
        );
        $response->assertStatus(200);

        $this->assertDatabaseHas('comments', $data);
        $resp = json_decode($response->getContent());
        $this->assertEquals($data['article_id'], $resp->data->article->id);
        $this->assertEquals($data['body'], $resp->data->body);
        $this->assertEquals($data['user_id'], $resp->data->user->id);
    }

    /**
     * A basic unit test example.
     * @group comments
     * @return void
     */
    public function testFailedUpdateCommentIfNotAuth()
    {
        $article = factory(Article::class)->create();
        $comment = factory(Comment::class)->create();

        $data = [
            'article_id' => $article->id,
            'body' => $this->faker->text,
            'user_id' => $this->user->id
        ];
        $response = $this->json(
            'put',
            route('comments.update', ['comment' => $comment]),
            $data
        );
        $response->assertStatus(401);
    }

    /**
     * A basic unit test example.
     * @group comments
     * @return void
     */
    public function testSuccessDeleteCommentIfAuth()
    {
        $this->actingAs($this->user, 'api');
        $comment = factory(Comment::class)->create();

        $response = $this->json(
            'delete',
            route('comments.destroy', ['comment' => $comment])
        );
        $response->assertStatus(204);
    }

    /**
     * A basic unit test example.
     * @group comments
     * @return void
     */
    public function testFailedDeleteCommentIfNotAuth()
    {
        $comment = factory(Comment::class)->create();

        $response = $this->json(
            'delete',
            route('comments.destroy', ['comment' => $comment])
        );
        $response->assertStatus(401);
    }
}
