<?php

namespace Tests\Unit;

use App\Article;
use Tests\TestCase;

class ArticleTest extends TestCase
{
    /**
     * A basic unit test example.
     * @group articles
     * @return void
     */
    public function testExample()
    {
        $this->assertTrue(true);
    }

    /**
     * A basic unit test example.
     * @group articles
     * @return void
     */
    public function testSuccessShowAllArticles()
    {
        $response = $this->json('get', route('articles.index'));
        $response->assertStatus(200);
    }

    /**
     * A basic unit test example.
     * @group articles
     * @return void
     */
    public function testSuccessCreateArticleIfAuth()
    {
        $this->actingAs($this->user, 'api');
        $data = [
            'title' => $this->faker->word,
            'body' => $this->faker->text,
            'user_id' => $this->user->id
        ];

        $response = $this->json('post', route('articles.store'), $data);

        $response->assertCreated();
        $this->assertDatabaseHas('articles', $data);
        $resp = json_decode($response->getContent());
        $this->assertEquals($data['title'], $resp->data->title);
        $this->assertEquals($data['body'], $resp->data->body);
        $this->assertEquals($data['user_id'], $resp->data->user->id);
    }

    /**
     * A basic unit test example.
     * @group articles
     * @return void
     */
    public function testFailedCreateArticleIfNotAuth()
    {
        $data = [
            'title' => $this->faker->word,
            'body' => $this->faker->text,
            'user_id' => $this->user->id
        ];

        $response = $this->json('post', route('articles.store'), $data);

        $response->assertStatus(401);
    }

    /**
     * A basic unit test example.
     * @group articles
     * @return void
     */
    public function testTitleRequiredValidationIfAuth()
    {
        $this->actingAs($this->user, 'api');
        $data = [
            'title' => '',
            'body' => $this->faker->text,
            'user_id' => $this->user->id
        ];
        $response = $this->json(
            'post',
            route('articles.store'),
            $data
        );
        $response->assertStatus(422);

        $this->assertDatabaseMissing('articles', $data);
        $resp = json_decode($response->getContent());
        $this->assertTrue(array_key_exists('message', (array)$resp));
        $this->assertTrue(array_key_exists('errors', (array)$resp));
        $this->assertTrue(array_key_exists('title', (array)$resp->errors));
        $this->assertFalse(array_key_exists('body', (array)$resp->errors));
        $this->assertFalse(array_key_exists('user_id', (array)$resp->errors));
    }

    /**
     * A basic unit test example.
     * @group articles
     * @return void
     */
    public function testTitleMinCharactersValidationIfAuth()
    {
        $this->actingAs($this->user, 'api');
        $data = [
            'title' => 'a',
            'body' => $this->faker->text,
            'user_id' => $this->user->id
        ];
        $response = $this->json(
            'post',
            route('articles.store'),
            $data
        );
        $response->assertStatus(422);

        $this->assertDatabaseMissing('articles', $data);
        $resp = json_decode($response->getContent());
        $this->assertTrue(array_key_exists('message', (array)$resp));
        $this->assertTrue(array_key_exists('errors', (array)$resp));
        $this->assertTrue(array_key_exists('title', (array)$resp->errors));
        $this->assertFalse(array_key_exists('body', (array)$resp->errors));
        $this->assertFalse(array_key_exists('user_id', (array)$resp->errors));
    }

    /**
     * A basic unit test example.
     * @group articles
     * @return void
     */
    public function testBodyRequiredValidationIfAuth()
    {
        $this->actingAs($this->user, 'api');
        $data = [
            'title' => $this->faker->word,
            'body' => '',
            'user_id' => $this->user->id
        ];
        $response = $this->json(
            'post',
            route('articles.store'),
            $data
        );
        $response->assertStatus(422);

        $this->assertDatabaseMissing('articles', $data);
        $resp = json_decode($response->getContent());
        $this->assertTrue(array_key_exists('message', (array)$resp));
        $this->assertTrue(array_key_exists('errors', (array)$resp));
        $this->assertTrue(array_key_exists('body', (array)$resp->errors));
        $this->assertFalse(array_key_exists('title', (array)$resp->errors));
        $this->assertFalse(array_key_exists('user_id', (array)$resp->errors));
    }

    /**
     * A basic unit test example.
     * @group articles
     * @return void
     */
    public function testBodyMinCharactersValidationIfAuth()
    {
        $this->actingAs($this->user, 'api');
        $data = [
            'title' => $this->faker->word,
            'body' => 'a',
            'user_id' => $this->user->id
        ];
        $response = $this->json(
            'post',
            route('articles.store'),
            $data
        );
        $response->assertStatus(422);

        $this->assertDatabaseMissing('articles', $data);
        $resp = json_decode($response->getContent());
        $this->assertTrue(array_key_exists('message', (array)$resp));
        $this->assertTrue(array_key_exists('errors', (array)$resp));
        $this->assertTrue(array_key_exists('body', (array)$resp->errors));
        $this->assertFalse(array_key_exists('title', (array)$resp->errors));
        $this->assertFalse(array_key_exists('user_id', (array)$resp->errors));
    }

    /**
     * A basic unit test example.
     * @group articles
     * @return void
     */
    public function testUserIdRequiredValidationIfAuth()
    {
        $this->actingAs($this->user, 'api');
        $data = [
            'title' => $this->faker->word,
            'body' => $this->faker->text,
            'user_id' => ''
        ];
        $response = $this->json(
            'post',
            route('articles.store'),
            $data
        );
        $response->assertStatus(422);

        $this->assertDatabaseMissing('articles', $data);
        $resp = json_decode($response->getContent());
        $this->assertTrue(array_key_exists('message', (array)$resp));
        $this->assertTrue(array_key_exists('errors', (array)$resp));
        $this->assertTrue(array_key_exists('user_id', (array)$resp->errors));
        $this->assertFalse(array_key_exists('title', (array)$resp->errors));
        $this->assertFalse(array_key_exists('body', (array)$resp->errors));
    }

    /**
     * A basic unit test example.
     * @group articles
     * @return void
     */
    public function testUserIdExistsInDatabaseValidationIfAuth()
    {
        $this->actingAs($this->user, 'api');
        $data = [
            'title' => $this->faker->word,
            'body' => $this->faker->text,
            'user_id' => '3333333333333333333333333333333'
        ];
        $response = $this->json(
            'post',
            route('articles.store'),
            $data
        );
        $response->assertStatus(422);

        $this->assertDatabaseMissing('articles', $data);
        $resp = json_decode($response->getContent());
        $this->assertTrue(array_key_exists('message', (array)$resp));
        $this->assertTrue(array_key_exists('errors', (array)$resp));
        $this->assertTrue(array_key_exists('user_id', (array)$resp->errors));
        $this->assertFalse(array_key_exists('title', (array)$resp->errors));
        $this->assertFalse(array_key_exists('body', (array)$resp->errors));
    }

    /**
     * A basic unit test example.
     * @group articles
     * @return void
     */
    public function testSuccessShowArticleIfAuth()
    {
        $this->actingAs($this->user, 'api');
        $article = factory(Article::class)->create();

        $response = $this->json(
            'get',
            route('articles.show', ['article' => $article])
        );
        $response->assertStatus(200);
        $resp = json_decode($response->getContent());
        $this->assertEquals($article->title, $resp->data->title);
        $this->assertEquals($article->body, $resp->data->body);
        $this->assertEquals($article->user_id, $resp->data->user->id);
    }

    /**
     * A basic unit test example.
     * @group articles
     * @return void
     */
    public function testFailedShowArticleIfNotAuth()
    {
        $article = factory(Article::class)->create();

        $response = $this->json(
            'get',
            route('articles.show', ['article' => $article])
        );
        $response->assertStatus(401);
    }

    /**
     * A basic unit test example.
     * @group articles
     * @return void
     */
    public function testSuccessUpdateArticleIfAuth()
    {
        $this->actingAs($this->user, 'api');
        $article = factory(Article::class)->create();

        $data = [
            'title' => $this->faker->word,
            'body' => $this->faker->text,
            'user_id' => $this->user->id
        ];
        $response = $this->json(
            'put',
            route('articles.update', ['article' => $article]),
            $data
        );
        $response->assertStatus(200);

        $this->assertDatabaseHas('articles', $data);
        $resp = json_decode($response->getContent());
        $this->assertEquals($data['title'], $resp->data->title);
        $this->assertEquals($data['body'], $resp->data->body);
        $this->assertEquals($data['user_id'], $resp->data->user->id);
    }

    /**
     * A basic unit test example.
     * @group articles
     * @return void
     */
    public function testFailedUpdateArticleIfNotAuth()
    {
        $article = factory(Article::class)->create();

        $data = [
            'title' => $this->faker->word,
            'body' => $this->faker->text,
            'user_id' => $this->user->id
        ];
        $response = $this->json(
            'put',
            route('articles.update', ['article' => $article]),
            $data
        );
        $response->assertStatus(401);
    }

    /**
     * A basic unit test example.
     * @group articles
     * @return void
     */
    public function testSuccessDeleteArticleIfAuth()
    {
        $this->actingAs($this->user, 'api');
        $article = factory(Article::class)->create();

        $response = $this->json(
            'delete',
            route('articles.destroy', ['article' => $article])
        );
        $response->assertStatus(204);
    }

    /**
     * A basic unit test example.
     * @group articles
     * @return void
     */
    public function testSuccessDeleteArticleIfNotAuth()
    {
        $article = factory(Article::class)->create();

        $response = $this->json(
            'delete',
            route('articles.destroy', ['article' => $article])
        );
        $response->assertStatus(401);
    }
}
